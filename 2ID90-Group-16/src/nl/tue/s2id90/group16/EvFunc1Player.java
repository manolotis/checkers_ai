package nl.tue.s2id90.group16;

import evalutations.EvaluationFunctionV1;

public class EvFunc1Player extends BasePlayer {

    public EvFunc1Player() {
        super(new EvaluationFunctionV1());
    }
}
