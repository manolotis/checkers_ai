package nl.tue.s2id90.group16;

import evalutations.EvaluationFunctionV2_2;

public class EvFunc2_2Player extends BasePlayer {

    public EvFunc2_2Player() {
        super(new EvaluationFunctionV2_2());
    }
}
