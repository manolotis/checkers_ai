package nl.tue.s2id90.group16;

import evalutations.EvaluationFunctionV3;

public class EvFunc3Player extends BasePlayer {

    public EvFunc3Player() {
        super(new EvaluationFunctionV3());
    }
}
