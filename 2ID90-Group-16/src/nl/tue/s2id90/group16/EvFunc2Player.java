package nl.tue.s2id90.group16;

import evalutations.EvaluationFunctionV2;

public class EvFunc2Player extends BasePlayer {

    public EvFunc2Player() {
        super(new EvaluationFunctionV2());
    }
}
