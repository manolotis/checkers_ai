package nl.tue.s2id90.group16;

import evalutations.EvaluationFunctionV4;

public class EvFunc4Player extends BasePlayer {

    public EvFunc4Player() {
        super(new EvaluationFunctionV4());
    }
}
