package nl.tue.s2id90.group16;

/**
 * Exception to stop the algorithm that searches for the best move when the time
 * is up
 */
public class AIStoppedException extends Exception {

    public AIStoppedException() {
    }
}
