/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.tue.s2id90.group16;

import nl.tue.s2id90.draughts.DraughtsState;
import org10x10.dam.game.Move;

/**
 *
 * @author s134668
 */

//Represents a given state of the game
public class GameNode {

    private DraughtsState gameState;
    private Move bestMove;
    private int value;

    /**
     * Constructor
     *
     * @param state The game state of the node
     */
    public GameNode(DraughtsState state) {
        gameState = state;
    }

    public DraughtsState getGameState() {
        return gameState;
    }
    
    public void setBestMove(Move move) {
        bestMove = move;
    }

    public Move getBestMove() {
        return bestMove;
    }    
    
    public void setValue(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }

}
