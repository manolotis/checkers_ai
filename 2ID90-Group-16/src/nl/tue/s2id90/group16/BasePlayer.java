package nl.tue.s2id90.group16;

import evalutations.BaseEvaluation;
import java.util.ArrayList;
import java.util.List;
import nl.tue.s2id90.draughts.DraughtsState;
import nl.tue.s2id90.draughts.player.DraughtsPlayer;
import nl.tue.s2id90.game.GameState;
import org10x10.dam.game.Move;

/**
 *
 * @author s134668
 */
/**
 * Base checkers player that uses AlphaBeta pruning with a given evaluation
 * function to find the best move possible It is necessary to provide an
 * evaluation function when instantiating this class
 */
public abstract class BasePlayer extends DraughtsPlayer {

    GameNode bestNode = null;
    BaseEvaluation evaluation;
    int value = 0;
    boolean stopped = false;
    boolean busy = false;

    public BasePlayer(BaseEvaluation evaluation) {
        super(Checkernator.class.getResource("resources/checkernator.png"));
        this.evaluation = evaluation;
    }

    @Override
    public Move getMove(DraughtsState state) {
        GameNode node = new GameNode(state);
        evaluation.setWhite(state.isWhiteToMove());

        try {
            /**
             * Iterative deepening so we do not get into an infinite (or just
             * too large) depth.
             */
            for (int i = 1; i < Integer.MAX_VALUE; i++) {
                alphaBeta(node, Integer.MIN_VALUE, Integer.MAX_VALUE, i, true);
            }
        } catch (AIStoppedException ex) {
        }
        bestNode = node;
        return node.getBestMove();
    }

    @Override
    public void stop() {
        stopped = true;
    }

    @Override
    public Integer getValue() {
        if (bestNode == null) {
            return 0;
        } else {
            return bestNode.getValue();
        }
    }

    /**
     * @return the best move the player has been able to find
     */
    public Move getBestMove() {
        return bestNode.getBestMove();
    }

    /**
     * Regular alpha-beta search for a given GameNode
     *
     * @param node The node to which apply the alpha-beta search
     * @param alpha best value for maximizer found at the moment
     * @param beta best value for minimizer found at the moment
     * @param isMax whether it is a maximizer player or not
     * @return the value of the best move found before it is stopped
     * @throws AIStoppedException
     */
    int alphaBeta(GameNode node, int alpha, int beta, int depth, boolean isMax)
            throws AIStoppedException {
        busy = true;
        DraughtsState state = node.getGameState();

        //If a leave of the game state tree has been reached, an evaluation is executed
        if (depth == 0 || state.isEndState()) {
            return evaluation.evaluate(node.getGameState());
        }

        /**
         * Gets list of possible moves and sets the first one to be the first
         * one in such list
         */
        List<Move> moves = new ArrayList<Move>(state.getMoves());
        Move bestMove = moves.get(0);

        for (Move move : moves) {
            //If time is up, throw exception to get out of recursion
            if (stopped) {
                stopped = false;
                busy = false;
                throw new AIStoppedException();
            }
            /**
             * Apply the move and call alphaBeta recursively in order to find
             * it's child's best move, then undo the move initially applied
             */
            state.doMove(move);
            GameNode childNode = new GameNode(state);
            int childValue = alphaBeta(childNode, alpha, beta, depth - 1, !isMax);
            state.undoMove(move);

            //It is a maximizer
            if (isMax) {
                if (childValue > alpha) {
                    alpha = childValue;
                    bestMove = move;
                }
            } //It is a minimizer
            else {
                if (childValue < beta) {
                    beta = childValue;
                    bestMove = move;
                }
            }

            if (alpha >= beta) {
                if (isMax) {
                    return beta;
                } else {
                    return alpha;
                }
            }
        }
        //update bestMove
        node.setBestMove(bestMove);

        if (isMax) {
            node.setValue(alpha);
            return alpha;
        } else {
            node.setValue(beta);
            return beta;
        }
    }
}
