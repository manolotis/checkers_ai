package evalutations;

import nl.tue.s2id90.draughts.DraughtsState;

/**
 * Evaluation that takes into account the neighbours of each piece
 * DOES NOT WORK BETTER THAN V2
 * @author s134668
 */
public class EvaluationFunctionV2_2 extends EvaluationFunctionV1 {

    public EvaluationFunctionV2_2() {
        super();
    }

    public EvaluationFunctionV2_2(boolean isWhite) {
        super(isWhite);
    }

    //Evaluation is based on the number of pieces
    @Override
    public int evaluate(DraughtsState state) {
        return super.evaluate(state) + evaluateNeighbours(state);
    }

    /**
     *
     * @param state
     * @return
     */
    public int evaluateNeighbours(DraughtsState state) {
        int value = 0;
        //Don't include 0 and 9 for now to avoid null pointers and make it simpler
        for (int r = 1; r < 9; r++) {
            for (int c = 1; c < 9; c++) {
                int field = state.getPiece(r, c);
                //If field is empty or white, then check next field
                if (field == DraughtsState.EMPTY
                        || field == DraughtsState.WHITEFIELD) {
                    continue;
                }
                
                if(field == DraughtsState.WHITEPIECE){
                    if(state.getPiece(r - 1, c - 1) == DraughtsState.WHITEPIECE){
                        value = value + 20;
                    }
                    if(state.getPiece(r - 1, c + 1) == DraughtsState.WHITEPIECE){
                        value = value + 20;
                    }
                    if(state.getPiece(r + 1, c - 1) == DraughtsState.WHITEPIECE){
                        value = value + 20;
                    }
                    if(state.getPiece(r + 1, c + 1) == DraughtsState.WHITEPIECE){
                        value = value + 20;
                    }
                }
                if(field == DraughtsState.BLACKPIECE){
                    if(state.getPiece(r + 1, c - 1) == DraughtsState.BLACKPIECE){
                        value = value - 20;
                    }
                    if(state.getPiece(r + 1, c + 1) == DraughtsState.BLACKPIECE){
                        value = value - 20;
                    }
                    if(state.getPiece(r - 1, c + 1) == DraughtsState.BLACKPIECE){
                        value = value - 20;
                    }
                    if(state.getPiece(r - 1, c - 1) == DraughtsState.BLACKPIECE){
                        value = value - 20;
                    }
                }
            }
        }
        return isWhiteTurn ? value : -value;

    }
}
