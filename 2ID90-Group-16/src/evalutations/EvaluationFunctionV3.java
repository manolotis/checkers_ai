package evalutations;

import nl.tue.s2id90.draughts.DraughtsState;

/**
 * Evaluation that takes into account the number of pieces that are on the sides
 * and therefore cannot be eaten
 *
 * @author s134668
 */
public class EvaluationFunctionV3 extends EvaluationFunctionV2 {

    public EvaluationFunctionV3() {
        super();
    }

    public EvaluationFunctionV3(boolean isWhite) {
        super(isWhite);
    }

    //Evaluation is based on the number of pieces
    @Override
    public int evaluate(DraughtsState state) {
        return super.evaluate(state) + evaluateSides(state);
    }

    /**
     *
     * @param state
     * @return
     */
    public int evaluateSides(DraughtsState state) {
        int value = 0;

        //right column of board
        for (int i = 5; i < 50; i += 10) {
            if (state.getPiece(i) == DraughtsState.WHITEPIECE) {
                value += 20;
            }
            if (state.getPiece(i) == DraughtsState.BLACKPIECE) {
                value += -20;
            }
        }
        //left column of the board
        for (int i = 6; i < 50; i += 10) {
            if (state.getPiece(i) == DraughtsState.WHITEPIECE) {
                value += 20;
            }
            if (state.getPiece(i) == DraughtsState.BLACKPIECE) {
                value += -20;
            }
        }
        return isWhiteTurn ? value : -value;
    }
}
