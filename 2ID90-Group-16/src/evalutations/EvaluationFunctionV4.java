package evalutations;

import nl.tue.s2id90.draughts.DraughtsState;

/**
 * Evaluation that takes into account the number of pieces that are in the last
 * row, therefore preventing the opponent from creating a King piece
 *
 * @author s134668
 */
public class EvaluationFunctionV4 extends EvaluationFunctionV3 {

    public EvaluationFunctionV4() {
        super();
    }

    public EvaluationFunctionV4(boolean isWhite) {
        super(isWhite);
    }

    //Evaluation is based on the number of pieces
    @Override
    public int evaluate(DraughtsState state) {
        return super.evaluate(state) + evaluateHomeRow(state);
    }

    /**
     *
     * @param state
     * @return
     */
    public int evaluateHomeRow(DraughtsState state) {
        int value = 0;

        //top row
        for (int i = 1; i < 6; i ++) {
            if (state.getPiece(i) == DraughtsState.BLACKPIECE) {
                value += -50;
            }
        }
        //bottom row
        for (int i = 46; i < 51; i ++) {
            if (state.getPiece(i) == DraughtsState.WHITEPIECE) {
                value += 50;
            }
        }
        return isWhiteTurn ? value : -value;
    }
}
