package evalutations;

import nl.tue.s2id90.draughts.DraughtsState;

/**
 *
 * @author s134668
 */
public abstract class BaseEvaluation {
    //Whether a player is white or not
    protected boolean isWhiteTurn;

    /**
     * Constructor. Default value true
     */
    public BaseEvaluation() {
        isWhiteTurn = true;
    }

    /**
     * Constructor
     *
     * @param isWhite whether or not the player using the evaluation function is
     * white
     */
    public BaseEvaluation(boolean isWhite) {
        this.isWhiteTurn = isWhite;
    }

    /**
     * Set whether or not the player is white
     *
     * @param isWhite whether or not the player is white
     */
    public void setWhite(boolean isWhite) {
        this.isWhiteTurn = isWhite;
    }

    /**
     * Gets whether or not the player is white
     *
     * @return whether or not the player is white
     */
    public boolean getWhite() {
        return isWhiteTurn;
    }

    /**
     * Evaluates "goodness" of a given state
     * 
     * @param state State to evaluate
     * @return the score of the evaluation for the given state
     */
    public abstract int evaluate(DraughtsState state);
}
