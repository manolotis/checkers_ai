package evalutations;

import nl.tue.s2id90.draughts.DraughtsState;

/**
 *
 * @author s134668
 */
public class EvaluationFunctionV1 extends BaseEvaluation {

    public EvaluationFunctionV1() {
        super();
    }

    public EvaluationFunctionV1(boolean isWhite) {
        super(isWhite);
    }

    //Evaluation is based on the number of pieces
    @Override
    public int evaluate(DraughtsState state) {
        return evaluatePieceNumber(state.getPieces());
    }

    /**
     * Gives score of 100 for each piece of own color, subtracts it for
     * opponent's color; Kings count three times more
     *
     * @param pieces pieces to evaluate
     * @return score after evaluating
     */
    public int evaluatePieceNumber(int[] pieces) {
        int value = 0;
        for (int piece : pieces) {
            switch (piece) {
                case DraughtsState.WHITEPIECE:
                    value += 100;
                    break;
                case DraughtsState.BLACKPIECE:
                    value += -100;
                    break;
                case DraughtsState.WHITEKING:
                    value += 300;
                    break;
                case DraughtsState.BLACKKING:
                    value += -300;
                    break;
            }
        }
        return isWhiteTurn ? value : -value;
    }
}
